#-----------------------------------------------------
# Slac4M Class- Shapefile Line Adjacency Calculator - Mat101 HS2015
# Version 4.1: Modularity, Eigenvector, Betweeness, rtree, class
# Date: January 19, 2016
# Project Idea by Martin Tomko
# Authors: Merlin Unterfinger, Manuel Luck, Marius Voegtli
#-----------------------------------------------------

import os                                                           # used for the saving of the memory in .txt-file
import fiona                                                        # used for the opening of shapefiles.
from shapely.geometry import Point, shape, LineString, mapping      # used for the calculations in the shapefiles.
from collections import defaultdict, OrderedDict                    # used for crossing extraction, create new schemas
import rtree                                                        # used to create a range tree index
import igraph                                                       # used for create a network graph

class Slac4M(object):
    """ A class representing a shapefile for the calculation of adjacency measures, aim is to cluster street networks.

    """
    def __init__(self, inputfilename, outputfolderpath,
                 csvfile = True, orig_attr = False, crossings = False):

        """ The constructor.

        """
        # Specify path to input file.
        self.inputFile = inputfilename
        self.inputFileName = inputfilename.split('/')[-1]
        self.outputFileFolderPath = outputfolderpath

        # Specify keyword attributes
        self.crossings = crossings
        self.orig_attr = orig_attr
        self.csvfile = csvfile

        # Test variable for checking if method calculate was called before saving.
        self.calculated = False

    def initiate(self):
        """ Prepares the paths for the calcualtions and outputfiles.

        """
        # Create paths, filenames and folders for the shp files.
        shpFolderPath = self.outputFileFolderPath + 'Shp/'
        shpFileName_adjacency = self.inputFileName[0:-4] + "_adjacency.shp"
        self.shpFile_adjacency = os.path.join(shpFolderPath,shpFileName_adjacency)

        # Create paths, filenames and folders for the crossings.
        if self.crossings is True:
            self.shpFileName_crossings = self.inputFileName[0:-4] + "_crossings.shp"
            self.shpFile_crossings = os.path.join(shpFolderPath, self.shpFileName_crossings)
        try:
            os.makedirs(shpFolderPath)
        except OSError:
            print "Folder '"+shpFolderPath+"' already exists. Existing files will be overwritten."

        # Temporary file and folder paths for singlepart shapefile.
        sp_shpFolderPath = self.outputFileFolderPath + 'tmp/'
        shpFileName_singlepart = self.inputFileName[0:-4] + "_singlepart.shp"
        self.shpFile_singlepart = os.path.join(sp_shpFolderPath,shpFileName_singlepart)
        print self.shpFile_singlepart
        try:
            os.makedirs(sp_shpFolderPath)
        except OSError:
            print "Folder '"+sp_shpFolderPath+"' already exists. Existing files will be overwritten."

        # Create paths, filenames and folders for the csv files (if demanded).
        if self.csvfile is True:
            cvsFolderPath = self.outputFileFolderPath + 'Csv/'
            self.csvFileName_edgelist = self.inputFileName[0:-4] + "_edgelist.csv"
            self.csv_edgelist = os.path.join(cvsFolderPath, self.csvFileName_edgelist)

            self.csvFileName_adjacency = self.inputFileName[0:-4] + "_adjacency.csv"
            self.csv_adjacency = os.path.join(cvsFolderPath,self.csvFileName_adjacency)

            if self.crossings is True:
                self.csvFileName_crossings = self.inputFileName[0:-4] + "_crossings.csv"
                self.csv_crossings = os.path.join(cvsFolderPath, self.csvFileName_crossings)

            # Create new cvs folder.
            try:
                os.makedirs(cvsFolderPath)
            except OSError:
                print "Folder '"+cvsFolderPath+"' already exists. Existing files will be overwritten."

        print ""

    def calculate(self):
        """ This method calculates the relevant measures using Fiona, Shapely, RTree and iGraph"""

        #-----------------------------------------------------
        # Convert Multipart to Singlepart Features
        #-----------------------------------------------------

        print "Converting multipart to singlepart features..."

        # Open the original multipart file in "r"-mode.
        with fiona.open(self.inputFile) as input:

            # Extracting the schema, crs and driver of the original shapefile to build new shapefiles later.
            self.driver_shp = input.driver
            self.crs_shp = input.crs
            self.schema_shp = input.schema

            # Change the schema
            schema_update = self.schema_shp
            entry = {'Slac_ID': 'int'}
            schema_update["properties"].update(entry)
            i = 0

            # Create the new file: The schema changed, the driver and crs are the same.
            with fiona.open(self.shpFile_singlepart,'w',driver=input.driver, crs=input.crs, schema=input.schema) as output:
                # Read the input file
                for multi in input:
                    # Extract each polygon feature.
                    try:
                        for poly in shape(multi['geometry']):
                            # Create new feature-ID "Slac_ID".
                            entry = {'Slac_ID': i}
                            multi["properties"].update(entry)
                            # Write the polygon feature in shapefile.
                            output.write({'properties': multi['properties'],'geometry': mapping(poly)})
                            # Increase counter for each polygon.
                            i += 1

                    # Single-part feature get a "normal" ID.
                    except TypeError:
                        # Creating new Feature-ID "Slac_ID".
                        entry = {'Slac_ID': i}
                        multi["properties"].update(entry)
                        output.write({'properties': multi['properties'], 'geometry': multi['geometry']})
                        i += 1

        print "Done.", "\n"


        #-----------------------------------------------------
        # Extracting Line Intersections with a Range Tree Index
        #-----------------------------------------------------

        print "Extracting line intersections..."
        self.singlepart = list()
        self.edgelist = list()
        with fiona.open(self.shpFile_singlepart) as input:
            index = rtree.index.Index()
            # Fill index
            for f in input:
                self.singlepart.append({'properties': f['properties'], 'geometry': f['geometry']})
                fid = int(f["properties"]['Slac_ID'])
                g1 = shape(f['geometry'])
                index.insert(fid, g1.bounds)
                # End of filling index
            with fiona.open(self.shpFile_singlepart) as input2:
                n=0
                for f2 in input2:
                    g2 = shape(f2['geometry'])
                    for fid in list(index.intersection(g2.bounds)):
                        if fid != int(f2["properties"]['Slac_ID']):
                            f1 = input[fid]
                            g1 = shape(f1['geometry'])
                            if g1.intersects(g2):
                                n += 1
                                #print n, f2["properties"]['Slac_ID'], f1["properties"]['Slac_ID']
                                self.edgelist.append([f2["properties"]['Slac_ID'], f1["properties"]['Slac_ID']])

        print "Done.", "\n"



        #-----------------------------------------------------
        # Extracting Crossings with number of Crossing Streets
        #-----------------------------------------------------

        if self.crossings is True:
            print "Extracting number of lines per intersection..."
            points = list()
            for line in self.singlepart:
                # Save start and endpoint of the features in a list, together with the line ID = Slac_ID.
                a = [line['geometry']['coordinates'][0], line['geometry']['coordinates'][-1], line["properties"]['Slac_ID']]
                points.append(a)

            from collections import defaultdict
            self.crossing = defaultdict(list)

            # Assign the lines to the touching nodes.
            for point1, point2, Slac in points:
                self.crossing[point1].append(Slac)
            for point1, point2, Slac in points:
                self.crossing[point2].append(Slac)

            # Split the dict.
            self.keys = self.crossing.keys()
            self.values = self.crossing.values()

            print "Done.", "\n"



        #-----------------------------------------------------
        # Calculate Betweenness, Degree & Membership
        #-----------------------------------------------------

        print "Calculating measures for adjacency..."
        # Create edge list, needed format: list of tuples.
        edges_list = []
        vertex_list = []
        for elements in self.edgelist:
            edges_list.append((elements[0], elements[1]))
            vertex_list.append(elements[0])

        vertexnumber = max(vertex_list)+1

        # Fill the edge list into iGraph.
        g = igraph.Graph()
        g.add_vertices(vertexnumber)
        g.add_edges(edges_list)

        # Calculate betweenness, degree & cluster membership
        self.outdegree = g.degree()
        self.membership = g.clusters().membership
        self.outbetween = g.betweenness(vertices=None, directed=True, cutoff=10) # cutoff = 2
        self.edgebetween = g.edge_betweenness(directed=True, cutoff=10)
        self.eigenvector = g.community_leading_eigenvector().membership
        self.multilevel = g.community_multilevel().membership
        self.labelpropagation = g.community_label_propagation().membership
        self.infomap = g.community_infomap().membership

        print "Done.", "\n"

        print "Summary of iGraph:"
        print "     Vertices:   ", vertexnumber
        print "     Edges:      ", len(edges_list)
        print "     Min Degree: ", min(self.outdegree)
        print "     Max Degree: ", max(self.outdegree), "\n"

        self.calculated = True

    def save(self):

        if self.calculated:
            """ Save the calculated measures into shapefiles and csv-files if demanded."""
            #-----------------------------------------------------
            # Creating Shapefiles containing Adjacency Measures
            #-----------------------------------------------------

            print "Saving..."

            # LINES: Shapefile with the lines and the calculated measurement of adjacency.
            if self.orig_attr is False:
                # Create new schema.
                schema_adjacency = {'geometry': '3D LineString',
                                    'properties': OrderedDict([('Slac_ID', 'int'),
                                                               ('Slac_Degree', 'int'),
                                                               ('Slac_Cluster', 'int'),
                                                               ('Slac_Between', 'float'),
                                                               ('Slac_EdgeBetween', 'float'),
                                                               ('Slac_Eigen', 'int'),
                                                               ('Slac_Multilv', 'int'),
                                                               ('Slac_LabelProp', 'int'),
                                                               ('Slac_InfoMap', 'int')])}

                # Create the new shapefile: The schema changed, the driver and crs are the same.
                with fiona.open(self.shpFile_adjacency, 'w',driver=self.driver_shp, crs=self.crs_shp, schema=schema_adjacency) as output:
                    # Read the adjacency data & write to the output.
                    for line in self.singlepart:
                        n = line["properties"]["Slac_ID"]
                        output.write({'properties':
                                          {'Slac_ID': n, 'Slac_Degree': self.outdegree[n],
                                           'Slac_Cluster': self.membership[n],
                                           'Slac_Between': self.outbetween[n],
                                           'Slac_EdgeBetween' : self.edgebetween[n],
                                           'Slac_Eigen': self.eigenvector[n],
                                           'Slac_Multilv': self.multilevel[n],
                                           'Slac_LabelProp': self.labelpropagation[n],
                                           'Slac_InfoMap': self.infomap[n]},
                                      'geometry': line['geometry']})

                print "File '"+self.shpFile_adjacency+"' created."

            else:
                # Create the new file: The schema changed, the driver and crs are the same.
                schema_update = self.schema_shp
                #entry = {'Slac_ID': 'int'}
                entry = {'Slac_ID': 'int',
                         'Slac_Degree': 'int',
                         'Slac_Cluster': 'int',
                         'Slac_Between': 'float',
                         'Slac_EdgeBetween': 'float',
                         'Slac_Eigen': 'int',
                         'Slac_Multilv': 'int',
                         'Slac_LabelProp': 'int',
                         'Slac_InfoMap': 'int'}
                schema_update["properties"].update(entry)
                with fiona.open(self.shpFile_adjacency,'w',driver=self.driver_shp, crs=self.crs_shp, schema=schema_update) as output:
                    # Read the input file
                    for line in self.singlepart:
                        n = line["properties"]["Slac_ID"]
                        line['properties'].update({'Slac_Degree': self.outdegree[n]})
                        line['properties'].update({'Slac_Cluster': self.membership[n]})
                        line['properties'].update({'Slac_Between': self.outbetween[n]})
                        line['properties'].update({'Slac_EdgeBetween': self.edgebetween[n]})
                        line['properties'].update({'Slac_Eigen': self.eigenvector[n]})
                        line['properties'].update({'Slac_Multilv': self.multilevel[n]})
                        line['properties'].update({'Slac_LabelProp': self.labelpropagation[n]})
                        line['properties'].update({'Slac_InfoMap': self.infomap[n]})
                        output.write({'properties': line['properties'], 'geometry': line['geometry']})

                print "File '"+self.shpFile_adjacency+"' created.", "\n"


            if self.crossings is False:
                print ""
            else:
                # POINTS: Shapefile with the crossings and the number of touching lines.
                # Updating the schema of the original file.
                schema_crossings = { 'geometry': 'Point',
                'properties': {'Slac_Degree': 'int',}}

                # Create the new shapefile: The schema changed, the driver and crs are the same.
                with fiona.open(self.shpFile_crossings,'w',driver=self.driver_shp, crs=self.crs_shp, schema=schema_crossings) as output:
                    # Read the crossings-data & write to the output.
                    for i in range(len(self.crossing)):
                        point = Point(float(self.keys[i][0]), float(self.keys[i][1]))
                        output.write({'properties': {'Slac_Degree': len(self.values[i])}, 'geometry': mapping(point)})

                print "File '"+self.shpFile_crossings+"' created.", "\n"



            #-----------------------------------------------------
            # Writing Results in csv.files (if demanded)
            #-----------------------------------------------------

            if self.csvfile is True:

                print "Saving .csv file(s)..."
                # Create new csv file.
                with open(self.csv_edgelist, 'w') as csv:
                    # Header csv.
                    csv.write("Slac_ID_line1"+";"+"Slac_ID_line2"+ "\n")
                    # Data csv1.
                    for element in self.edgelist:
                        csv.write(
                            str(element[0]) + ";" +  # Slac_ID line 1
                            str(element[1]) + "\n")  # Slac_ID line 2
                    print "File '"+self.csvFileName_edgelist+"' created."

                # Create new csv file.
                with open(self.csv_adjacency, 'w') as csv:
                    # Header csv.
                    csv.write("Slac_ID"+";"+
                               "Slac_Degree"+";"+
                               "Slac_Cluster"+";"+
                               "Slac_Between"+";"+
                               "Slac_EdgeBetween"+";"+
                               "Slac_Eigen"+";"+
                               "Slac_Multilv"+";"+
                               "Slac_LabelProp"+";"+
                               "Slac_InfoMap"+
                               "\n")
                    # Data csv.
                    for line in self.singlepart:
                        n = line["properties"]["Slac_ID"]
                        csv.write(
                            str(n) + ";" +  # Slac_ID
                            str(self.outdegree[n]) + ";" +  # Degree of the Lines
                            str(self.membership[n]) + ";" +  # Original Cluster Membership
                            str(self.outbetween[n]) + ";" +  # Betweeness
                            str(self.edgebetween[n]) + ";" +  # Edge - Betweeness
                            str(self.eigenvector[n]) + ";" +  #  Eigenvector Cluster
                            str(self.multilevel[n]) + ";" +  # Multilevel
                            str(self.labelpropagation[n]) + ";" +  # Labelprop
                            str(self.infomap[n]) + # Infomap
                            "\n")
                    print "File '"+self.csvFileName_adjacency+"' created."

                if self.crossings is False:
                    print ""
                else:
                    with open(self.csv_crossings, 'w') as csv:
                        # Header csv.
                        csv.write("X_coord"+";"+"Y_coord"+";"+"N_lines"+"\n")
                        # Data csv2.
                        for i in range(len(self.crossing)):
                            csv.write(
                                str(self.keys[i][0]) + ";" +  # X-Coordinate
                                str(self.keys[i][1]) + ";" +  # Y-Coordinate
                                str(len(self.values[i]))+"\n") # Number of touching line
                        print "File '"+self.csvFileName_crossings+"' created.", "\n"
        else:
            print "Do the calculation before trying to save the results. Call the method '.calculate()'"


    def __str__(self):
        ''' Prints the Filepath of the input file
        '''
        return self.inputFile
