# README #

Network analysis of traffic counts in 
Great Britain (2000-2015): Detecting significant differences in traffic volume depending on daytime and region on the average workday.

### What is this repository for? ###

* GEO872 Advanced Spatial Analysis I - Project
* v1.1
* last change: 16. January 2017

### Authors ###

* Manuel Luck
* Merlin Unterfinger
* Autumn season 2017
* Departement of Geography, UZH 

### Folder Structure ###

* /R/:              R Codes (v3.3.1)
* /Python/:         Python Codes (v2.7.10)
* /Data/:           CSV Files
* /Data/SHP/:       Shapefiles
* /Figures/:        Figures Showing the Results of the different Steps
* /Documentation/:  Contains the Proposal, Presentation-Slides and the final Report